XDEBUG_HOST=$(shell ifconfig docker0 | grep 'inet' | head -1 | cut -d' ' -f10 | awk '{print $$1}')
ENVIRONMENT ?= dev
COMPOSE_PROJECT ?= secret_santa
COMPOSE_PROJECT_NAME ?= ${COMPOSE_PROJECT}_${ENVIRONMENT}
DOCKER_COMPOSE = XDEBUG_HOST=$(XDEBUG_HOST) docker-compose -p "$(COMPOSE_PROJECT_NAME)" -f etc/docker/docker-compose.$(ENVIRONMENT).yml
DOCKER_YARN ?= docker run --rm -v "$(PWD)":/app -w /app jonbaldie/yarn
COMPOSER ?= docker run --rm --interactive --tty -v "$(PWD)":/app --user $(id -u):$(id -g) composer:latest

buildup:
	$(DOCKER_COMPOSE) up --build -d

build:
	$(DOCKER_COMPOSE) build

up:
	$(DOCKER_COMPOSE) up -d

down:
	$(DOCKER_COMPOSE) down --remove-orphans

logs:
	$(DOCKER_COMPOSE) logs -f

ps:
	$(DOCKER_COMPOSE) ps

install-dev: set-dev-env install-front install-back

set-dev-env:
	$(eval ENVIRONMENT=dev)

install-front:
	$(DOCKER_YARN) /bin/sh -c "/root/.yarn/bin/yarn && node_modules/.bin/gulp build"

install-back:
	$(COMPOSER) install --no-scripts

update-back:
	$(COMPOSER) update --no-scripts

deploy:
	$(COMPOSER) install -o --no-dev --no-scripts
	bin/console c:c --env=prod
	serverless deploy --stage=prod
