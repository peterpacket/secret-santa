<?php


namespace App\Http;


use App\BadEmailException;
use App\DuplicateParticipantsException;
use App\EmailAddress;
use App\InvalidAmountException;
use App\Name;
use App\NoMatch;
use App\NotEnoughParticipantsException;
use App\Participant;
use App\ParticipantList;
use App\ParticipantsCanNotBeMatchedException;
use App\SecretSanta;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class SendController
{
    /**
     * @var SecretSanta
     */
    private SecretSanta $secretSanta;

    public function __construct(SecretSanta $secretSanta)
    {
        $this->secretSanta = $secretSanta;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(Request $request): Response
    {
        if ($_POST['password'] !== 'roodkapje') {
            return new Response('Unauthorized', 401);
        }
        try {
            $participants = new ParticipantList();
            foreach ($_POST['user_name'] AS $i => $name) {
                $participants = $participants->add(Participant::createNew(
                    Name::fromString($_POST['user_name'][$i]),
                    EmailAddress::fromString($_POST['user_email'][$i]),
                    $_POST['no_match'][$i] ? NoMatch::withEmailAddress(EmailAddress::fromString($_POST['no_match'][$i])) : NoMatch::empty()
                ));
            }

            if (count($participants) > 30) {
                die("Sorry, but due to risk of spamming this script is limited on only allow secret santa's of up to 30 people.");
            }

            $amount = (int)$_POST['amount'];

            $this->secretSanta->run($participants, $amount);

            return new Response('test');

        } catch (BadEmailException $e) {
            echo $e->getMessage();exit;
        } catch (ParticipantsCanNotBeMatchedException $e) {
            echo 'Matching failed';exit;
        } catch (DuplicateParticipantsException $e) {
        } catch (InvalidAmountException $e) {
        } catch (NotEnoughParticipantsException $e) {
        }

        return new Response('test');
    }
}