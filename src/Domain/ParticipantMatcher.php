<?php


namespace App;


class ParticipantMatcher
{

    /**
     * @param ParticipantList<Participant> $participants
     * @return Matches<MatchedPair>
     * @throws MatchingFailedException
     */
    public function match(ParticipantList $participants): Matches
    {
        $participants = $participants->randomized();

        $givers = $participants;
        $receivers = $participants;

        $matches = new Matches();
        $matches = $this->doMatch($givers, $receivers, $matches);

        return $matches;
    }

    /**
     * @param ParticipantList<Participant> $givers
     * @param ParticipantList<Participant> $receivers
     * @param Matches<MatchedPair> $matches
     * @return Matches<MatchedPair>
     * @throws MatchingFailedException
     */
    private function doMatch(ParticipantList $givers, ParticipantList $receivers, Matches $matches): Matches
    {
        if (count($givers) === 0) {
            return $matches;
        }

        $possibleReceivers = $receivers;
        $giver = $givers->first();
        $matched = false;

        while (count($possibleReceivers) > 0 && !$matched) {
            $receiver = $possibleReceivers->getRandomParticipant();

            try {
                $match = MatchedPair::betweenGiverAndReceiver($giver, $receiver);
                $matches = $matches->add($match);
                $givers = $givers->remove($giver);
                $receivers = $receivers->remove($receiver);

                $matches = $this->doMatch($givers, $receivers, $matches);
                $matched = true;
            } catch (MatchingFailedException $e) {
                $possibleReceivers = $possibleReceivers->remove($receiver);
                $receivers = $receivers->add($receiver);
            } catch (ParticipantsCanNotBeMatchedException $e) {
                $possibleReceivers = $possibleReceivers->remove($receiver);
            }
        }

        if (!$matched) {
            throw new MatchingFailedException();
        }

        return $matches;
    }
}