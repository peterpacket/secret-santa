<?php

namespace App;
/*
 * PHP Secret Santa
 * A very simple PHP based Secret Santa Script.
 *
 * @Author Carl Saggs (2011) (Adapted by Peter Packet)
 * @license MIT License
 *
 * Basic Usage:
 *	$santa = new SecretSanta();
 *	$santa->run(
 *			array(
 *					array('name'=>'Bob','email'=>'bob@bobnet.com'),
 *					array('name'=>'Dave','email'=>'dave@davenet.com')
 *				)
 *			);
 */

class SecretSanta
{

    /**
     * @var ParticipantMatcher
     */
    private $participantMatcher;

    /**
     * @var Notifier
     */
    private Notifier $notifier;

    /**
     * SecretSanta constructor.
     * @param ParticipantMatcher $participantMatcher
     */
    public function __construct(ParticipantMatcher $participantMatcher, Notifier $notifier)
    {
        $this->participantMatcher = $participantMatcher;
        $this->notifier = $notifier;
    }


    /**
     * @param ParticipantList<Participant> $participants
     * @throws DuplicateParticipantsException
     * @throws InvalidAmountException
     * @throws NotEnoughParticipantsException
     * @throws MatchingFailedException
     */
    public function run(ParticipantList $participants, int $amount): void
    {
        $this->guardMinimumParticipants($participants);
        $this->guardUniqueParticipants($participants);
        $this->guardValidAmount($amount);

        $matchedParticipants = $this->participantMatcher->match($participants);

        $this->notifier->notify($matchedParticipants, $amount);
    }

    /**
     * @param ParticipantList<Participant> $participants
     * @throws NotEnoughParticipantsException
     */
    private function guardMinimumParticipants(ParticipantList $participants): void
    {
        //Ensure that more than 2 users have been provided
        if (count($participants) < 2) {
            throw new NotEnoughParticipantsException();
        }
    }

    /**
     * @param ParticipantList<Participant> $participants
     * @throws DuplicateParticipantsException
     */
    private function guardUniqueParticipants(ParticipantList $participants): void
    {
        //Check there are no duplicate emails
        $emails = [];
        foreach ($participants as $participant) {
            /** @var Participant $participant */
            if (in_array($participant->getEmail(), $emails, true)) {
                throw new DuplicateParticipantsException();
            }
            $emails[] = $participant->getEmail();
        }
    }

    private function guardValidAmount(int $amount):void
    {
        if ($amount <= 0) {
            throw new InvalidAmountException();
        }
    }
}