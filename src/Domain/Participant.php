<?php


namespace App;


final class Participant
{

    private Name $name;

    private EmailAddress $emailAddress;

    private NoMatch $noMatch;

    private function __construct()
    {
    }

    public static function createNew(Name $name, EmailAddress $emailAddress, NoMatch $noMatc): Participant
    {
        $participant = new static;
        $participant->name = $name;
        $participant->emailAddress = $emailAddress;
        $participant->noMatch = $noMatc;

        return $participant;
    }

    public function canGiveTo(Participant $other): bool
    {
        if ($this->noMatch->blocks($other->emailAddress)) {
            return false;
        }

        if ($this->emailAddress->asString() === $other->emailAddress->asString()) {
            return false;
        }

        return true;
    }

    public function getEmail(): EmailAddress
    {
        return $this->emailAddress;
    }

    public function getNoMatch(): NoMatch
    {
        return $this->noMatch;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function __toString(): string
    {
        return $this->emailAddress;
    }

}