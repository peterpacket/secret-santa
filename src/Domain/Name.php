<?php


namespace App;


final class Name
{

    private string $name;

    private function __construct() {}

    public static function fromString(string $nameString): Name
    {
        $name = new static;
        $name->name = (string) preg_replace('/[^a-zA-Z0-9\w\-]/i', "", $nameString); //Remove all funncy characters

        return $name;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}