<?php


namespace App;


final class NoMatch
{
    private ?EmailAddress $emailAddress;

    private function __construct() {}

    public static function withEmailAddress(EmailAddress $email): NoMatch
    {
        $noMatch = new static();
        $noMatch->emailAddress = $email;

        return $noMatch;
    }

    public static function empty(): NoMatch
    {
        $noMatch = new static();
        $noMatch->emailAddress = null;

        return $noMatch;
    }

    public function blocks(EmailAddress $emailAddress): bool
    {
        if ($this->emailAddress === null) {
            return false;
        }

        return $emailAddress->asString() === $this->emailAddress->asString();
    }
}