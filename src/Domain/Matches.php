<?php


namespace App;

/**
 * @implements \IteratorAggregate<int, MatchedPair>
 */
class Matches implements \IteratorAggregate, \Countable
{

    /**
     * @var array<MatchedPair>
     */
    private array $values;

    public function __construct()
    {
        $this->values = [];
    }

    /**
     * @return \Iterator<MatchedPair>
     */
    public function getIterator(): \Iterator
    {
        return new \ArrayIterator($this->values);
    }

    public function count(): int
    {
        return count($this->values);
    }

    /**
     * @return Matches<MatchedPair>
     */
    public function add(MatchedPair $match): Matches
    {
        $new = clone($this);
        $values = array_merge($this->values, [$match]);
        $new->values = $values;

        return $new;
    }
}