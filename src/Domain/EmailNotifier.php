<?php

namespace App;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class EmailNotifier implements Notifier
{
    public function __construct(private readonly MailerInterface $mailer)
    {
    }

    public function notify(Matches $matches, int $amount): void
    {
        foreach ($matches as $match) {
            /** @var MatchedPair $match */
            $giver = $match->getGiver();
            $receiver = $match->getReceiver();

            $this->mailer->send(
                (new TemplatedEmail())
                    ->from(new Address('dekerstman@ppdev.eu', 'De Kerstman'))
                    // ->to($giver->getEmail()->asString())
                    ->to('peterpacket@gmail.com')
                    ->subject('Secret Santa ' . $giver->getName())
                    ->htmlTemplate('mails/invitation.html.twig')
                    ->context([
                        'giver' => $giver->getName(),
                        'receiver' => $receiver->getName(),
                        'amount' => $amount,
                    ])
                    ->bcc('peterpacket+secretsanta@gmail.com')
            );
        }
    }
}