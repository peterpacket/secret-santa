<?php


namespace App;


final class EmailAddress
{

    private string $emailAddress;

    private function __construct() {}

    /**
     * @throws BadEmailException
     */
    public static function fromString(string $email): EmailAddress
    {
        if (empty($email)) {
            throw new BadEmailException('Empty email');
        }
        if (strpos($email, '@') === false) {
            throw new BadEmailException('Contains no @');
        }

        $participantEmail = new static();
        $participantEmail->emailAddress = $email;

        return $participantEmail;
    }

    public function asString(): string
    {
        return $this->emailAddress;
    }

    public function __toString(): string
    {
        return $this->asString();
    }
}