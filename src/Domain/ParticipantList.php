<?php


namespace App;


use Exception;
use Traversable;

/**
 * @implements \IteratorAggregate<int, Participant>
 */
final class ParticipantList implements \IteratorAggregate, \Countable
{

    /**
     * @var array<Participant>
     */
    protected array $values = [];

    /**
     * @return \Iterator<Participant>
     */
    public function getIterator(): \Iterator
    {
        return new \ArrayIterator($this->values);
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->values);
    }

    /**
     * @return self<Participant>
     */
    public function add(Participant $participant): self
    {
        $new = new static();
        $new->values = array_merge($this->values, [$participant]);

        return $new;
    }

    /**
     * @return self<Participant>
     */
    public function remove(Participant $participant): self
    {
        $new = new static;
        $values = $this->values;

        if (($key = array_search($participant, $values, false)) !== false) {
            unset($values[$key]);
        }

        $new->values = array_values($values);
        return $new;
    }

    /**
     * @return ParticipantList<Participant>
     */
    public function randomized(): ParticipantList
    {
        $randomized = new static();
        $values = $this->values;
        shuffle($values);
        $randomized->values = $values;

        return $randomized;
    }

    public function getRandomParticipant():Participant
    {
        $randomKey = random_int(0, count($this->values) - 1);
        return $this->values[$randomKey];
    }

    public function first(): Participant
    {
        return $this->values[0];
    }

    /**
     * @return array<string>
     */
    public function getNames(): array
    {
        return array_map(fn(Participant $p) => $p->getName()->__toString(), $this->values);
    }
}