<?php


namespace App;


class PrinterNotifier implements Notifier
{

    /**
     * @param Matches<MatchedPair> $matches
     * @param int $amount
     */
    public function notify(Matches $matches, int $amount): void
    {
        foreach ($matches as $match) {
            /** @var MatchedPair $match */
            if ($match->getGiver()->getNoMatch()->blocks($match->getReceiver()->getEmail())) {
                echo 'WARNING NOMATCH ** ';
            }
            if ($match->getGiver()->getEmail()->asString() === $match->getReceiver()->getEmail()->asString()) {
                echo 'WARNING TOSELF ** ';
            }
            echo $match . '<br/>';
        }
    }
}