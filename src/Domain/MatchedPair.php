<?php


namespace App;


final class MatchedPair
{

    private Participant $giver;

    private Participant $receiver;

    private function __construct()
    {
    }

    /**
     * @throws ParticipantsCanNotBeMatchedException
     */
    public static function betweenGiverAndReceiver(Participant $giver, Participant $receiver): MatchedPair
    {
        if (!$giver->canGiveTo($receiver)) {
            throw new ParticipantsCanNotBeMatchedException();
        }

        $match = new static;
        $match->giver = $giver;
        $match->receiver = $receiver;

        return $match;
    }

    public function __toString(): string
    {
        return $this->giver->getEmail() . ' -> ' . $this->receiver->getEmail();
    }

    public function getGiver(): Participant
    {
        return $this->giver;
    }

    public function getReceiver(): Participant
    {
        return $this->receiver;
    }


}