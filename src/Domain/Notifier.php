<?php


namespace App;


interface Notifier
{

    /**
     * @param Matches<MatchedPair> $matches
     * @param int $amount
     */
    public function notify(Matches $matches, int $amount): void;

}