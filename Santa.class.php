<?php
/*
 * PHP Secret Santa
 * A very simple PHP based Secret Santa Script.
 *
 * @Author Carl Saggs (2011) (Adapted by Peter Packet)
 * @license MIT License
 *
 * Basic Usage:
 *	$santa = new SecretSanta();
 *	$santa->run(
 *			array(
 *					array('name'=>'Bob','email'=>'bob@bobnet.com'),
 *					array('name'=>'Dave','email'=>'dave@davenet.com')
 *				)
 *			);
 */
Class SecretSanta {
	//Vars
	private $item_value = 5;
	private $mail_from = 'De kerstman< kerstman@kerstmis.com>';
	private $mail_title = 'Secret Santa';
	//Logging
	private $sent_emails = array();
	
	/**
	 * Run
	 * runs the secret santa script on an array of users. 
	 * Everyone is assigned their secret santa and emailed with who they need to buy for.
	 * @param $users Array
	 * @return success
	 */
	public function run($users_array){
		//Check array is safe to use
		$ok = $this->validateArray($users_array);
		if(!$ok) return false;
		//If no issues, run!
		$matched = $this->assign_users($users_array);
		$this->sendEmails($matched);
		return true;
	}
	
	/**
	 * Validate Array
	 * Ensure array is safe to use in Secret Santa Script
	 * @param Users Array
	 * @return true if safe.
	 */
	private function validateArray($users_array){
		//Ensure that more than 2 users have been provided
		if(sizeof($users_array)<2){
			echo '[Error] A minimum of 2 secret santa participants is required in order to use this system.';
			return false;
		}
		//Check there are no duplicate emails
		$tmp_emails = array();
		foreach($users_array as $u){
			if(in_array($u['email'],$tmp_emails)){
				echo "[Error] Users cannot share an email or be in the secret santa more than once.";
				return false;
			}
			$tmp_emails[] = $u['email'];
		}
		return true;
	}
	/**
	 * Set the title of secret santa emails sent.
	 * @param $title
	 */
	public function setTitle($title){
		$this->mail_title = $title;
	}
	
	/**
	 * Set the price secret santa items should be around
	 * @param $price (in £'s)
	 */
	public function setAmount($price){
		$this->item_value = $price;
	}
	
	/**
	 * Set who your want the email to be sent from
	 * @param $name Name of Sender (e.g. Santa)
	 * @param $email Email of Sender (e.g. Santa@somedomain.com)
	 */
	public function setFrom($name,$email){
		$this->mail_from = "{$name} < {$email} >";
	}
	
	/**
	 * Assign every user in the array their secret santa
	 * Ensuring that everyone is assigned randomly and doesn't get themselves
	 *
	 * @param array of users
	 * @return array of assigned users
	 */
	private function assign_users($users_array){
		//$users = array(array())
        shuffle($users_array);
		$givers     = $users_array;
		$receivers  = $users_array;

        //var_dump($givers, $receivers);
        //echo'<br/>';
		//Foreach giver
		foreach($givers as $uid => $user){
            //echo '<b>Giver: ' . $user['name'] . '</b><br/>';
			$not_assigned = true;
			//While a user hasn't been assigned their secret santa
			while($not_assigned){
				//Randomly pick a person for the user to buy for
				$choice = rand(0, sizeof($receivers)-1);

                //echo 'Choice: ' . $receivers[$choice]['name'] . '<br/>';
				//If randomly picked user is NOT themselves
				if($user['email'] !== $receivers[$choice]['email']){
                    //If picked user is the no_match of the current giver
                    if($user['no_match'] == $receivers[$choice]['email']) {
                        //echo 'no match<br/>';
                        if(sizeof($receivers) == 1){
                            //echo 'oei, ik zen laatste<br/>';
                            //Swap with someone else (iterate over all persons untill it matches)
                            for($i=0; $i < count($givers); $i++) {

                                //echo 'switchen met: ' . $i . '-' . $givers[$i]['giving_to']['name'] . '<br/>';
                                if($user['no_match'] != $givers[$i]['giving_to']['email']
                                    && $user['email'] != $givers[$i]['giving_to']['email']
                                    && $givers[$i]['no_match'] != $receivers[$choice]['email']
                                    && $givers[$i]['email'] != $receivers[$choice]['email']) {

                                    $givers[$uid]['giving_to'] = $givers[$i]['giving_to'];
                                    $givers[$i]['giving_to'] = $receivers[$choice];
                                    $not_assigned = false;
                                    break;
                                }
                            }
                        }
                    } else {
                        //echo 'we have a match<br/>';
                        //Assign the user the randomly picked user
                        $givers[$uid]['giving_to'] = $receivers[$choice];
                        //And remove them from the list
                        unset($receivers[$choice]);
                        //Correct array
                        $receivers = array_values($receivers);
                        //exit loop
                        $not_assigned = false;
                    }
					
                    
                }else{
					//If we are the last user left and have been given ourselfs
                    //echo 'woops, picked myself<br/>';
					if(sizeof($receivers) == 1){
                        //echo 'last person, switching<br/>';
						//Swap with someone else (in this case the first guy who got assigned.
						//Steal first persons, person and give self to them.
						/*$givers[$uid]['giving_to'] = $givers[0]['giving_to'];
						$givers[0]['giving_to'] = $givers[$uid];
						$not_assigned = false;*/
                        for($i=0; $i < count($givers); $i++) {
                            //echo 'switchen met: ' . $i . '-' . $givers[$i]['giving_to']['name'] . '<br/>';
                            if($user['no_match'] != $givers[$i]['giving_to']['email']
                                && $user['email'] != $givers[$i]['giving_to']['email']
                                && $givers[$i]['no_match'] != $receivers[$choice]['email']
                                && $givers[$i]['email'] != $receivers[$choice]['email']) {

                                $givers[$uid]['giving_to'] = $givers[$i]['giving_to'];
                                $givers[$i]['giving_to'] = $receivers[$choice];
                                $not_assigned = false;
                                break;
                            }
                        }
					} 
				}

                //echo '------------------<br/>';
			}
		}
        if(!$this->checkUserArray($givers)) {
            echo 'invalid. Booo!';
            exit;
        }
		//Return array of matched users
		return $givers;
	}

    /**
     * Check wether the array of givers is valid, so they don't give to themelves and don't give to their no-match person.
     * @param $users
     * @return bool
     */
    public function checkUserArray($users) {
        foreach($users AS $user) {
            if($user['email'] == $user['giving_to']['email']) {
                return false;
            }
            if($user['no_match'] == $user['giving_to']['email']) {
                return false;
            }
        }
        return true;
    }
	
	/**
	 * Send Emails
	 * Emails all matched users with details of who they should be buying for.
	 * @param $matched users
	 */
	private function sendEmails($assigned_users){
		//For each user
		foreach($assigned_users as $giver){
            //echo $giver['name'] . ' => ' . $giver['giving_to']['name'] . '<br/>';
			//Send the following email
			$this->setTitle('Secret Santa ' . $giver['name']);
			$email_body = "Dag {$giver['name']}, 
			
			
Ik heb een groot probleem. Mijn rendieren hebben te veel gluhwein op en mijn elfen staken dit jaar voor betere werkomstandigheden. Bovendien is mijn slee aan een dringend onderhoud toe. Hierdoor zal ik dit jaar geen tijd hebben om iedereen een cadeautje te bezorgen. Daarom geef ik u de volgende opdracht:
				
Geef een cadeautje aan {$giver['giving_to']['name']}

Uw cadeautje mag ongeveer €{$this->item_value} kosten,


Vrolijk kerstfeest en een welgemeende Ho-Ho-Ho,
				
De Kerstman"; 
			//Log that its sent
			$this->sent_emails[] = $giver['email'];
			//Send em via normal PHP mail method
			mail($giver['email'], $this->mail_title, $email_body, "From: {$this->mail_from}\r\n");
	        mail("peterpacket+secretsanta@gmail.com", $this->mail_title, $email_body, "From: {$this->mail_from}\r\n");
			
			$fileName = 'berichten/' . $giver['name'] . '.txt';
			$file = fopen($fileName, 'w+');
			fwrite($file, $email_body);
			fclose($file);
			
			//echo $giver['name'] . ' => ' . $giver['giving_to']['name'] . '<br/>';
		}	
	}
	
	/**
	 * Get Sent Emails
	 * Return the list of emails that have been sent via the script
	 * @return Array of emails
	 */
	public function getSentEmails(){
		return $this->sent_emails;
	}
}