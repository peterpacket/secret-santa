var tmp_dom = null;
var counter = 1;

function extraUser() {
    //Store tmp object for creation of more (if one isnt already stored)
    if (tmp_dom == null) tmp_dom = document.getElementById('template').children[0];

    //Create new node
    new_node = tmp_dom.cloneNode(true);
    //Blank inputs and update form ID's
    inputs = new_node.getElementsByTagName('input')
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].value = '';
        inputs[i].name = inputs[i].name.slice(0, -1) + counter;
    }
    //Add new row
    document.getElementById('insert_zone').appendChild(new_node);
    //Update Counters
    counter++;
    document.getElementById('count').value = counter;
    //Stop form submitting
    return false;
}

function addParticipant(name, email, nomatch) {
    var participant = [name, email, nomatch]
    //Store tmp object for creation of more (if one isnt already stored)
    if (tmp_dom == null) tmp_dom = document.getElementById('template').children[0];

    //Create new node
    new_node = tmp_dom.cloneNode(true);
    //Blank inputs and update form ID's
    inputs = new_node.getElementsByTagName('input')
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].value = participant[i];
    }
    //Add new row
    document.getElementById('insert_zone').appendChild(new_node);
    //Update Counters
    counter++;
    document.getElementById('count').value = counter;
    //Stop form submitting
    return false;
}

function removeRow(me) {
    //Store so we can add new ones (assumings its not already there)
    if (tmp_dom == null) tmp_dom = document.getElementById('insert_zone').children[0];
    document.getElementById('insert_zone').removeChild(me.parentNode);
}

window.onload = function () {
    addParticipant('Moeke', 'marijkevansanden@gmail.com', 'packetwim@gmail.com')
    addParticipant('Vake', 'packetwim@gmail.com', 'marijkevansanden@gmail.com')
    addParticipant('Hans', 'hans.packet@gmail.com', 'evidebuyser@gmail.com')
    addParticipant('Evi', 'evidebuyser@gmail.com', 'hans.packet@gmail.com')
    addParticipant('Geert', 'geert.packet@gmail.com', 'sofie_van_der_haegen@hotmail.com')
    addParticipant('Sofie', 'sofie_van_der_haegen@hotmail.com', 'geert.packet@gmail.com')
    addParticipant('Stijn', 'stijn.packet@gmail.com', '')
    addParticipant('Peter', 'peterpacket@gmail.com', 'sonia.barrani@gmail.com')
    addParticipant('Sonia', 'sonia.barrani@gmail.com', 'peterpacket@gmail.com')
}